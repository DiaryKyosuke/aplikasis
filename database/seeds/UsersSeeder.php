<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create('id_ID');
      $id_user = \App\Pengguna::create([
        'username' => 'login',
        'email' => $faker->email,
        'role' => 1,
        'status' => 1,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ])->id_user;

      \App\User::create([
        'id' => $id_user,
        'name' => 'Admin',
        'username' => 'login',
        'password' => bcrypt('energeek'),
        'remember_token' => str_random(60),
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
  }
