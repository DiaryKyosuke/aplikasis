<table border="2px">
<tr>
	<td colspan="5">Nama Barang : {{$deta->nama_barang}}</td>
</tr>
<tr>
	<td colspan="4">Periode : {{$awal}} s/d {{$akhir}}</td>
</tr>
<tr></tr>
	<tr>
		<td rowspan="2">No.</td>
		<td rowspan="2">Tanggal</td>
		<td colspan="2">Jumlah</td>
	</tr>
	<tr>
		<td>Masuk</td>
		<td>Keluar</td>
	</tr>
	@foreach($dets as $det)
	<tr>
		<td>{{++$i}}</td>
		<td>{{$det->tanggal}}</td>

		@if ($det->jenis == 1)
		<td>{{$det->jumlah}}</td>
		<td></td>
		@elseif ($det->jenis == 0)
		<td></td>
		<td>{{$det->jumlah}}</td>
		@endif
	</tr>
	@endforeach
	<tr>
		<td></td>
		<td>Total</td>
		<td>=SUM(C6:C{{$i+5}})</td>
		<td>=SUM(D6:D{{$i+5}})</td>
	</tr>
	<tr>
		<td></td>
		<td>Sisa</td>
		<td colspan="2">=C{{$i+6}}-D{{$i+6}}</td>
	</tr>
</table>