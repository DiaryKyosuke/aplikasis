<script type="text/javascript">
  $('.date').datepicker({  

    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
  });  
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>
<!-- append -->
<script type="text/javascript">
  $(document).ready(function(){
    var i= {{$i}};
    $('.tambah').click(function(){  
      i++;
      $('tbody').append(
        '<tr id="baris'+i+'">'+
        
        '<td width="40%"><select style="width: 100%;" class="form-control select22 kedua" id="'+i+'" name="id_barang[]"><option></option>@foreach($benda as $barangs)<option value="{{$barangs->id_barang}}" stoks="{{$barangs->stok}}" harga="{{number_format($barangs->harga)}}">{{$barangs->nama_barang}}</option>@endforeach</select>'+
        '<input style="width: 100%;" name="id[]" type="hidden" readonly class="form-control mb-2 idbar'+i+'">'+
        '</td>'+

        '<td width="15%"><input style="width: 100%;" type="text" readonly name="stok[]" class="form-control mb-2" id="stoks'+i+'">'+

        '<input style="width: 100%;" type="hidden" name="stox[]" readonly class="form-control mb-2">'+
        '</td>'+

        '<td width="18%"><input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah" autocomplete="off">'+
        
        '<input style="width: 100%;" type="hidden" name="jums[]" class="form-control mb-2" value="0">'+
        '<input style="width: 100%;" type="hidden" name="jum[]" readonly class="form-control mb-2">'+
        '</td>'+

        '<td><input type="text" name="harga[]" id="harga'+i+'" class="form-control mb-2" readonly></td>'+

        '<td><a href="#" class="btn btn-sm btn-danger mb-2 hapus" id="'+i+'"><i class="fas fa-minus"></i></a></td>'+
        '</tr>'
        );
      $(".select22").select2({
        placeholder: "Nama Barang",
        allowClear: true
      });
      $(document).on('click', '.hapus', function(){  
        var button_id = $(this).attr("id");
        $('#baris'+button_id+'').remove();
      }); 

      $('.kedua').on("change", function(){  
        var select_id = $(this).attr("id"); 
        var stoks = $('#'+select_id+' option:selected').attr('stoks');
        var harga = $('#'+select_id+' option:selected').attr('harga');
        var text = $('#'+select_id+' option:selected').val();
        $('#stoks'+select_id+'').val(stoks);
        $('#harga'+select_id+'').val(harga);
        $('.idbar'+select_id+'').val(text);
        s = select_id - 1;
        se = select_id - 2;
        sel = select_id - 3;
        sele = select_id - 4;
        selec = select_id - 5;
        select = select_id - 6;
        selecte = select_id - 7;
        selected = select_id - 8;
        selecteda = select_id - 9;
        selectedab = select_id - 10;
        if ($(".id_bar1").val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($(".id_bar2").val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($(".id_bar3").val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($(".id_bar4").val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($(".id_bar5").val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($(".id_bar6").val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($(".id_bar7").val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($(".id_bar10").val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+s+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }

        else if ($('.idbar'+se+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+sel+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+sele+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+selec+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+select+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+selecte+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+selected+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+selecteda+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
        else if ($('.idbar'+selectedab+'').val() == $('.idbar'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
      });
});
});
</script>
<!-- endappend -->
<script type="text/javascript">
  $(document).ready(function(){
    // select2
    <?php foreach ($dets as $det): ?>
      var q = {{$b++}};
      $('#select'+q+'').select2({
        placeholder: "Nama Barang",
        allowClear: true
      });
    <?php endforeach ?>

    // pemanggilan atribut ke form  
    <?php foreach ($dets as $det): ?>
      var c{{++$d}} = {{$c++}};
      $('#select'+c{{$d}}+'').on("change", function(){
        var stok = $('#select'+c{{$d}}+' option:selected').attr('stoka'+c{{$d}}+'');
        var jumlah = $('#select'+c{{$d}}+' option:selected').attr('jumlah'+c{{$d}}+'');
        var harga = $('#select'+c{{$d}}+' option:selected').attr('harga'+c{{$d}}+'');
        $('#stok'+c{{$d}}+'').val(stok);
        $('#harga'+c{{$d}}+'').val(harga);
        $('#jumlah'+c{{$d}}+'').val(jumlah);
      });
    <?php endforeach ?> 

    // hapus baris append
    <?php foreach ($dets as $det): ?>
      var hapus{{++$hapus}} = {{$terhapus++}};
      $('#del'+hapus{{$hapus}}+'').on('click', function(){ 
        $('#baris'+hapus{{$hapus}}+'').remove();  
      }); 
    <?php endforeach ?>
  });
</script>