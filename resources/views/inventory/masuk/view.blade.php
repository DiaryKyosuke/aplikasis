@extends('layouts.detailinventorymasuk')
@section('content')
<br>
<div class="m-portlet m-portlet--full-height">
	<div class="m-portlet__body">						
		<div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_5" role="tablist">
			<div class="accordion" id="accordionExample">
				<div class="m-accordion__item m-accordion__item--danger">
					<div class="m-accordion__item-head collapse" id="headingOne" class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<span class="m-accordion__item-icon"><i class="far fa-calendar-alt"></i></span>
						<span class="m-accordion__item-title"><font size="1%">{{ Carbon\Carbon::parse($transaksis->tanggal)->formatLocalized('%A, %d %B %Y')}}</font></span>
						<span class="m-accordion__item-mode"></span>
					</div>

					<div id="collapseOne" class="collapse show m-accordion__item-body" aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body">
							<table border="1" class="table table-striped- table-bordered table-hover table-checkable">
								<thead>
									<tr class="text-center">
										<th width="5px"><font size="1%" class="font-weight-bold">No.</font></th>
										<th width="25%"><font size="1%" class="font-weight-bold">Nama Barang</font></th>
										<th width="20%"><font size="1%" class="font-weight-bold">Harga Satuan</font></th>
										<th width="15%"><font size="1%" class="font-weight-bold">Stok</font></th>
										<th width="15%"><font size="1%" class="font-weight-bold">Jumlah</font></th>
										<th width="25%"><font size="1%" class="font-weight-bold">Harga Total</font></th>
									</tr>
								</thead>
								<tbody>
									@foreach($dets as $detail)
									<tr class="text-center">
										<td><font size="1%">{{++$i}}</font></td>
										<td><font size="1%">{{$detail->nama_barang}}</font></td>
										<td>
											<font size="1%">Rp. {{number_format($detail->harga)}}</font></td>
										<td><font size="1%">{{$detail->stok}}</font></td>
										<td><font size="1%">{{$detail->jumlah}}</font></td>
										<td>Rp. {{number_format($detail->harga * $detail->jumlah)}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
						</div>
					</div>
					
				</div>
			</div>
			<font size="1%">
				<label class="font-weight-bold">Keterangan :</label>
				{{$transaksis->keterangan}}
			</font>
		</div>
	</div>
</div>	
@endsection