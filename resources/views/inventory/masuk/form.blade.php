@extends('layouts.inventorymasuk')
@section('title','Form Inventory Barang Masuk')
@section('masuk')
<div class="container">
	@include('layouts.alert_form_null')


	<div class="row justify-content-center">
		<div class="col-lg-10">
			<div class="m-portlet">
				<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30 alert-danger" role="alert">
					<div class="m-alert__icon">
						<i class="fas fa-exclamation-circle"></i>
					</div>
					<div class="m-alert__text">
						Sebelum Disimpan Pastikan Nama Barang Tidak Ada Yang Sama!!
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row justify-content-center">
		<div class="col-lg-10">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Form Tambah Data Barang Masuk
							</h3>
						</div>
					</div>
				</div>

				<!--begin::Form-->
				<form method="post" action="{{url('inventory/masuk/store')}}" class="m-form m-form--label-align-right">
					@csrf
					<div class="m-portlet__body">
						<div class="m-form__section m-form__section--first">
							<div class="form-row align-items-center">
								<div class="col-auto">
									<div class="col-md-12">
										<strong>Tanggal </strong>
									</div>
								</div>
								<div class="col-auto">
									<div class="col-md-12">
										<div class="input-group">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="la la-calendar glyphicon-th"></i></div>
											</div>
											<input type="text" class="date form-control" name="tanggal" autocomplete="off" placeholder="Tanggal">
											<!-- hidden id_transaksi dan jenis -->
											<input type="hidden" name="jenis" value="1">

										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="form-row align-items-center">
								<div class="col-auto">
									<div class="col-md-12">
										<strong>Keterangan </strong>
									</div>
								</div>
								<div class="col-auto">
									<div class="col-md-12">
										<div class="input-group">
											<textarea  rows="3" name="keterangan"></textarea>
										</div>
									</div>
								</div>
							</div>
							<br>


							<div class="col-md-12">
								<table class="table table-borderless" width="100%"> 
									<thead>
										<tr>
											<td><strong>Barang</strong></td>
											<td><strong>Stok</strong></td>
											<td><strong>Jumlah</strong></td>
											<td><strong>Harga</strong></td>
										</tr>
									</thead>
									<tbody>
										<tr>  
											<td width="40%">
												<select style="width: 100%;" class="form-control select22" name="id_barang[]" id="element">
													<option></option>
													@foreach($barang as $barangs)
													<option value="{{$barangs->id_barang}}" harga="{{number_format($barangs->harga)}}" stok="{{$barangs->stok}}">{{$barangs->nama_barang}}</option>
													@endforeach
												</select>
											</td> 
											<input type="hidden" class="nama_barang" value="">
											<input type="hidden" name="permission" value="terbuka">
											<td width="15%">
												<input style="width: 100%;" type="text" name="stok[]" readonly class="form-control mb-2 stok">
											</td>
											<td width="18%">
												<input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah" autocomplete="off">
											</td>
											<td>
												<input type="text" name="harga[]" readonly id="harga" class="form-control mb-2">
											</td>

											<td>
												<a href="#" class="btn btn-sm btn-info mb-2 tambah"><i class="la la-plus"></i></a>
											</td>  
										</tr>
									</tbody>
								</table>  
							</div>
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<button type="submit" class="btn btn-sm btn-primary" >Simpan</button>
									<a href="{{ url('inventory.masuk.grid') }}" class="btn btn-sm btn-success">Back</a>
								</div>
							</div>
						</div>
					</div>
				</form>

				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>
@include('layouts.js_tambah_inventory_masuk')
@endsection
