@extends('layouts.views')
@section('content')
<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Tanggal Transaksi
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
       {{Carbon\Carbon::parse($transaksi->tanggal)->formatLocalized('%A, %d %B %Y')}}
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Keterangan Transaksi
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        {{$transaksi->keterangan}}
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Tabel Transaksi
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <table border="1" class="table table-striped- table-bordered table-hover table-checkable">
              <thead >
                <tr class="text-center">
                  <th width="20px"><font size="1%">No.</font></th>
                  <th width="40%"><font size="1%">Nama Barang</font></th>
                  <th width="40%"><font size="1%">Harga Satuan</font></th>
                  <th width="40%"><font size="1%">Stok</font></th>
                  <th width="40%"><font size="1%">Jumlah</font></th>
                  <th width="40%"><font size="1%">Harga Total</font></th>
                </tr>
              </thead>
              <tbody>
                @foreach($detail as $barang)
                  <tr>
                    <td><font size="1%"></font>{{++$i}}.</td>
                    <td><font size="1%">{{$barang->nama_barang}}</font></td>
                    <td><font size="1%">{{number_format($barang->harga)}}</font></td>
                    <td><font size="1%">{{$barang->stok}}</font></td>
                    <td><font size="1%">{{$barang->jumlah}}</font></td>
                    <td><font size="1%">Rp.{{number_format($barang->harga * $barang->jumlah)}}</font></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>                  
@endsection