@extends('layouts.masteruser')
@section('title','Form User')
@section('masteruser')
<div class="col-lg-12">
	<!--begin::Portlet-->
	@include('layouts.alert_form_null')
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Form User
					</h3>
				</div>
			</div>
		</div>

		<!--begin::Form-->
		<form method="post" action="{{url('master/user/store')}}" class="m-form m-form--label-align-right">
			@csrf
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Username:</label>
						<div class="col-lg-6">
							<input type="text" class="form-control m-input" placeholder="username" name="username" value="">
							<span class="m-form__help">Masukkan Username</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Email:</label>
						<div class="col-lg-6">
							<input type="email" class="form-control m-input" placeholder="Email" name="email" value="">
							<span class="m-form__help">Masukkan Email Baru</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-2">Role</label>
						<div class="col-lg-6">
							<select name="role" class="form-control m-input" id="element">
								<option value="1">Admin</option>
								<option value="2">User</option>
							</select>
							<span class="m-form__help">Pilih Role</span>
						</div>
						<input type="hidden" class="name" name="name" value="Admin">
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Status:</label>
						<div class="col-lg-6">
							<input type="hidden" name="status" value="0">
							<input data-switch="true" type="checkbox" checked="checked" data-on-color="info" data-on-text="Aktif" data-off-text="Tidak Aktif" data-off-color="danger" name="status" value="1">
							<br>
							<span class="m-form__help">Pilih Status</span>
						</div>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Password:</label>
						<div class="row">
							<div class="col">
								<input type="password" id="awal" class="form-control m-input password" placeholder="Password baru" value="">
								<span class="m-form__help">Masukkan Password Baru</span>
							</div>
							<div class="col">
								<input type="password" id="akhir" class="form-control password" placeholder="Konfirmasi Password" name="password">
								<span class="m-form__help">Konfirmasi Password</span>
							</div>
						</div>
					</div>
					<div class=" m-form__group row">
						<label class="col-lg-2 col-form-label"></label>
						<span><input type="checkbox" class="form-checkbox"> Lihat Kata Sandi</span>
					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							<button type="submit" class="btn btn-primary" id="submit">Tambah</button>
							<a href="{{url('master.user.grid')}}" class="btn btn-secondary">Batal</a>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('.password').attr('type','text');
			}else{
				$('.password').attr('type','password');
			}
		});
		$('#submit').click(function() {
			var pass = $('#awal').val();
			var pass2 = $('#akhir').val();						
			if (pass != pass2) {				
				Swal({
					type: 'error',
					title: 'Whoops! Password Konfirmasi Harus Sama Dengan Password Baru.',
					text: 'Failed Password'
				})
				return false;
			}
		});

		$("#element").on("change", function(){
			var text = $("#element option:selected").text();
			$(".name").val(text);
		}); 
	});
</script>
@endsection							