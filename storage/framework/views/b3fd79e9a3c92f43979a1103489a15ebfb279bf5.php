<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Login Aplikasi Dishub Analisa Dampak Lalu Lintas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:700|Roboto&display=swap" rel="stylesheet"> 
    
    <link rel="stylesheet" type="text/css" media="screen" href="masuk/plugin/caviar-dreams-cufonfonts-webfont/style.css" />
    
    <link rel="stylesheet" href="masuk/login/css/bootstrap.css">
    <link rel="stylesheet" href="masuk/login/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="masuk/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="masuk/login/css/main-login.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="masuk/login/css/main-login-swk.css" />


    <script src="masuk/login/js/bootstrap.js"></script>
    <script src="masuk/login/js/bootstrap.min.js"></script>
    <script src="masuk/login/js/jquery.js"></script>
    <script src="masuk/login/js/jquery.sticky.js"></script>
    <!-- <script src="main.js"></script> -->
</head>

<body>
    <div class="body">
        <div class="login">
            <div class="row">
                <div class="col-lg-12 left">
                    <!-- <p class="title">
                                Aplikasi Analisa<br>
                                Dampak Lalu Lintas
                            </p> -->
                            <img src="masuk/img/illustration/login-farfale-2.png" alt="" srcset="">
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-lg-12" style="height: 100vh">

                            <div class="border-login">
                                <div class="login-wrapper">
                                    <div class="top-logo">
                                        <p>F A R F A L E</p>
                                        <span>
                                            <!-- <img src="masuk/img/logo/logo-sby.png" alt="Bappeko Surabaya"> -->

                                            <!-- <img src="masuk/img/logo/swk-icon.png" alt="aplikasi sentra wisata kuliner"> -->
                                        </span>
                                        <span>
                                            <!-- <p>Dinas Perhubungan Kota Surabaya </p> -->
                                        </span>
                                    </div>
                                    <div class="inner-login-wrapper">


                                        <div class="form-login">
                                            <p class="title">
                                                Aplikasi Inventori
                                            </p>
                                            <div class="logo-app">
                                                <!-- <img src="masuk/img/logo/swk-icon.png" alt="aplikasi sentra wisata kuliner"> -->

                                            </div>
                                            <p class="sub-title">
                                                Selamat datang, silahkan masuk kembali
                                            </p>
                                            <form method="POST" action="<?php echo e(route('login')); ?>" autocomplete="off">
                                                <?php echo csrf_field(); ?>
                                                <div class="username form-group">
                                                    <label for="">Username</label>
                                                    <input class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" type="text" name="email" placeholder="Username" value="<?php echo e(old('username')); ?>">
                                                </div>
                                                <?php if($errors->has('email')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                                <div class="password form-group">
                                                    <label for="">Password</label>
                                                    <input class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" type="password" name="password" placeholder="Password" required>
                                                </div>
                                                <?php if($errors->has('password')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('password')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                                <div style="margin-bottom:250px">
                                                    <button type="submit" class="btn btn-grad-teal btn-block  btn-shadow-login-invert">
                                                        <h5 style="font-weight:bo">LOGIN</h5>
                                                    </button>
                                                    <a class="txt1" href="<?php echo e(route('register')); ?>">
                                                        Create new account
                                                        <i class="fa fa-long-arrow-right"></i>                      
                                                    </a>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- FOOTER COPYRIGHT -->
                    <!-- <div class="footer-bottom">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    
                                    <p>©2019 &middot;<a href="index.html"> Dinas Perhubungan Kota Surabaya </a> &middot;
                                        All rights
                                        reserved</p>
                                </div>
                            </div>
                        </div>
                    </div> -->

                </div>
            </div>
            

        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- <center> -->
                            <p>©2019 &middot;<a href="#"> F A R F A L E production</a> &middot;
                                All rights
                            reserved</p>
                            <!-- </center> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    </html>