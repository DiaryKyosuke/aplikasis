<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <?php if(session()->has('success')): ?>
                <div class="alert alert-success"><?php echo e(session('success')); ?></div>
                <?php endif; ?>
                <div class="card-header">Ubah Kata Sandi</div>

                <div class="card-body">
                    <form method="POST" action="<?php echo e(route('password.update')); ?>">
                     <?php echo e(csrf_field()); ?>

                     <?php echo e(method_field('put')); ?>


                     <div class="form-group<?php echo e($errors->has('current_password') ? ' has-error' : ''); ?> row">
                        <label for="current_password" class="col-md-4 col-form-label text-md-right">Sandi Lama</label>

                        <div class="col-md-6">
                            <input id="current_password" type="password" class="form-control password" name="current_password" autofocus>
                        </div>
                    </div>

                    <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?> row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Sandi Baru</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control password" name="password">
                        </div>
                    </div>

                    <div class="form-group <?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?> row">
                        <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">Konfirmasi Sandi Baru</label>

                        <div class="col-md-6">
                         <input id="password_confirmation" type="password" class="form-control password" name="password_confirmation">
                         <span><input type="checkbox" class="form-checkbox"> Lihat Kata Sandi</span>
                     </div>
                 </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                <a href="<?php echo e(url('/dasboard.aplikasi')); ?>" class="btn btn-sm btn-success">Batal</a>
                <button type="submit" id="submit" class="btn btn-sm btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>