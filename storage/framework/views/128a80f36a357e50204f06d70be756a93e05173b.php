<?php $__env->startSection('content'); ?>
<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Tanggal Transaksi
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
       <?php echo e(Carbon\Carbon::parse($transaksi->tanggal)->formatLocalized('%A, %d %B %Y')); ?>

      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Keterangan Transaksi
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <?php echo e($transaksi->keterangan); ?>

      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Tabel Transaksi
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <table border="1" class="table table-striped- table-bordered table-hover table-checkable">
              <thead >
                <tr class="text-center">
                  <th width="20px"><font size="1%">No.</font></th>
                  <th width="40%"><font size="1%">Nama Barang</font></th>
                  <th width="40%"><font size="1%">Harga Satuan</font></th>
                  <th width="40%"><font size="1%">Stok</font></th>
                  <th width="40%"><font size="1%">Jumlah</font></th>
                  <th width="40%"><font size="1%">Harga Total</font></th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $barang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td><font size="1%"></font><?php echo e(++$i); ?>.</td>
                    <td><font size="1%"><?php echo e($barang->nama_barang); ?></font></td>
                    <td><font size="1%"><?php echo e(number_format($barang->harga)); ?></font></td>
                    <td><font size="1%"><?php echo e($barang->stok); ?></font></td>
                    <td><font size="1%"><?php echo e($barang->jumlah); ?></font></td>
                    <td><font size="1%">Rp.<?php echo e(number_format($barang->harga * $barang->jumlah)); ?></font></td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>                  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.views', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>