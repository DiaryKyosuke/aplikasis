<?php $__env->startSection('title','Data Barang'); ?>
<?php $__env->startSection('masterbarang'); ?>
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
	<?php if(session('sukses')): ?>
	<div class="alert alert-success text-center" role="alert">
		<?php echo e(session('sukses')); ?>

	</div>
	<?php endif; ?>
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data Barang
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="<?php echo e(url('master.barang.form')); ?>" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah Barang</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_barang">
			<thead >
				<tr class="text-center">
					<th width="20px">No.</th>
					<th width="40%">Nama Barang</th>
					<th width="20%">Harga</th>
					<th width="20%">Stok</th>
					<th width="40%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $__currentLoopData = $data_barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $barang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(++$no); ?>.</td>
					<td><?php echo e($barang->nama_barang); ?></td>
					<td>Rp. <?php echo e(number_format($barang->harga)); ?></td>
					<td><?php echo e($barang->stok); ?></td>
					<td class="text-center">
						<a href="master.barang.<?php echo e($barang->id_barang); ?>.edit_barang" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
						
						<a href="master.barang.<?php echo e($barang->id_barang); ?>.grid" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus')"><i class="fa fa-trash"></i></a>

						<a  data-fancybox href="<?php echo e(url('uploadgambar')); ?>/<?php echo e($barang->gambar); ?>" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-camera-retro"></i></a>
					</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.masterbarang', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>