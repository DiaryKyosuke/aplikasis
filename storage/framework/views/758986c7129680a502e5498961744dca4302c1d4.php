<?php $__env->startSection('title','Form Inventory Barang Masuk'); ?>
<?php $__env->startSection('masuk'); ?>
<div class="container">
	<?php echo $__env->make('layouts.alert_form_null', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="row justify-content-center">
		<div class="col-lg-10">
			<div class="m-portlet">
				<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30 alert-danger" role="alert">
					<div class="m-alert__icon">
						<i class="fas fa-exclamation-circle"></i>
					</div>
					<div class="m-alert__text">
						Sebelum Disimpan Pastikan Nama Barang Tidak Ada Yang Sama!!
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row justify-content-center">
		<div class="col-lg-10">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Form Edit Data Barang Masuk
							</h3>
						</div>
					</div>
				</div>

				<!--begin::Form-->
				<form action="inventory/masuk/<?php echo e($transaksis->id_transaksi); ?>/update" method="post" class="m-form m-form--label-align-right">
					<?php echo csrf_field(); ?>
					<div class="m-portlet__body">
						<div class="m-form__section m-form__section--first">
							<div class="form-row align-items-center">
								<div class="col-auto">
									<div class="col-md-12">
										<strong>Tanggal </strong>
									</div>
								</div>
								<div class="col-auto">
									<div class="col-md-12">
										<div class="input-group">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="la la-calendar glyphicon-th"></i></div>
											</div>
											<input type="text" class="date form-control" name="tanggal" autocomplete="off" placeholder="Tanggal" value="<?php echo e(Carbon\Carbon::parse($transaksis->tanggal)->formatLocalized('%d-%m-%Y')); ?>">
											<!-- hidden jenis -->
											<input type="hidden" name="jenis" value="1">
											
										</div>
									</div>
								</div>
							</div>
							<br>

							<div class="form-row align-items-center">
								<div class="col-auto">
									<div class="col-md-12">
										<strong>Keterangan</strong>
									</div>
								</div>
								<div class="col-auto">
									<div class="col-md-12">
										<div class="input-group">
											<textarea class="mx-sm-3" rows="3" name="keterangan"><?php echo e($transaksis->keterangan); ?></textarea>
										</div>
									</div>
								</div>
							</div>
							<br>
							<input type="hidden" name="id_transaksi[]" value="<?php echo e($details->id_transaksi); ?>">
							<input type="hidden" name="permission" value="<?php echo e($transaksis->permission); ?>">
							<div class="col-md-12">
								<table class="table table-borderless" width="100%"> 
									<thead>
										<tr>
											<td><strong>Barang</strong></td>
											<td><strong>Stok</strong></td>
											<td><strong>Jumlah</strong></td>
											<td><strong>Harga</strong></td>
											<td>
												<a href="#" class="btn btn-sm btn-info mb-2 tambah"><i class="la la-plus"></i></a>
											</td>
										</tr>
									</thead>
									<tbody>
										<?php $__currentLoopData = $dets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deta): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<tr id="baris<?php echo e(++$i); ?>">
											<td width="40%">
												<select style="width: 100%;" class="form-control" name="id_barang[]" id="select<?php echo e($i); ?>">
													<?php $__currentLoopData = $bars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $barang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<?php if(($barang->id_barang == $deta->id_barang)): ?>
													<option value="<?php echo e($barang->id_barang); ?>" stoka<?php echo e($i); ?>="<?php echo e($barang->stok); ?>" jumlah<?php echo e($i); ?>="<?php echo e($deta->jumlah); ?>" harga<?php echo e($i); ?>="<?php echo e(number_format($barang->harga)); ?>" selected="true"><?php echo e($barang->nama_barang); ?>

													</option>
													<?php else: ?>
													<option value="<?php echo e($barang->id_barang); ?>" stoka<?php echo e($i); ?>="<?php echo e($barang->stok); ?>" jumlah<?php echo e($i); ?>="<?php echo e($barang->jumlah); ?>" harga<?php echo e($i); ?>="<?php echo e(number_format($barang->harga)); ?>"><?php echo e($barang->nama_barang); ?>

													</option>
													<?php endif; ?> 
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>

												<input style="width: 100%;" name="id[]" type="hidden" readonly class="form-control mb-2 id_bar<?php echo e($i); ?>" value="<?php echo e($deta->id_barang); ?>">
											</td>
											<td width="15%">
												<input style="width: 100%;" type="text" name="stok[]" readonly class="form-control mb-2" value="<?php echo e($deta->stok); ?>" id="stok<?php echo e($i); ?>">

												<input style="width: 100%;" type="hidden" name="stox[]" readonly class="form-control mb-2" value="<?php echo e($deta->stok); ?>">
											</td>
											<td width="18%"><input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" placeholder="Jumlah" id="jumlah<?php echo e($i); ?>" autocomplete="off" value="<?php echo e($deta->jumlah); ?>">


												<input style="width: 100%;" type="hidden" name="jums[]" class="form-control mb-2" value="1">
												<input style="width: 100%;" type="hidden" name="jum[]" readonly class="form-control mb-2" value="<?php echo e($deta->jumlah); ?>">
											</td>
											<td>
												<input type="text" name="harga[]" id="harga<?php echo e($i); ?>" value="<?php echo e(number_format($deta->harga)); ?>" class="form-control mb-2" readonly>
											</td>
											<td>
												<a href="#" class="btn btn-sm btn-danger mb-2" id="del<?php echo e($i); ?>"><i class="fa fa-minus"></i></a>
											</td>
										</tr>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</tbody>
								</table>  
							</div>
						</div>
					</div>

					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<a href="<?php echo e(url('inventory.masuk.grid')); ?>" class="btn btn-sm btn-success">Back</a>
									<button type="submit" class="btn btn-sm btn-primary">Simpan</button>
								</div>
							</div>
						</div>
					</div>
				</form>

				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>
<?php echo $__env->make('layouts.js_edit_inventory_masuk', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.inventorymasuk', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>