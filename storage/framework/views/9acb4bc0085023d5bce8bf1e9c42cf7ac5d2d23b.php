<?php $__env->startSection('title','Form Edit User'); ?>
<?php $__env->startSection('masteruser'); ?>
<div class="col-lg-12">
	<!--begin::Portlet-->
	<?php if($errors->any()): ?>
	<div class="alert alert-danger">
		<strong>Data ada yang kosong!!</strong> Tolong diperiksa kembali.<br>
		<ul>
			<?php $__currentLoopData = $errors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
	</div>
	<?php endif; ?>
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Form Edit User
					</h3>
				</div>
			</div>
		</div>

		<!--begin::Form-->
		<form class="m-form m-form--label-align-right" method="POST" action="master/user/<?php echo e($users->id_user); ?>/update">
			<?php echo csrf_field(); ?>
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Username:</label>
						<div class="col-lg-6">
							<input type="text" class="form-control m-input" placeholder="username" name="username" value="<?php echo e($users->username); ?>">
							<span class="m-form__help">Username Yang Sekarang</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Email:</label>
						<div class="col-lg-6">
							<input type="email" name="email" class="form-control m-input" placeholder="Email" value="<?php echo e($users->email); ?>">
							<span class="m-form__help">Email Yang Sekarang</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-2">Role</label>
						<div class="col-lg-6">
							<select name="role" class="form-control" id="element">
								<option value="1" <?php if($users->role == '1'): ?>selected <?php endif; ?>>Admin</option>
								<option value="2" <?php if($users->role == '2'): ?>selected <?php endif; ?>>User</option>
							</select>
							<span class="m-form__help">Role Yang Sekarang</span>
							<input type="hidden" class="name" name="name" <?php if($users->role == '1'): ?> value="Admin" <?php else: ?> value="User" <?php endif; ?> >
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Status:</label>
						<div class="col-lg-6">
							<?php if($users->status == '1'): ?>
							<input type="hidden" name="status" value="0">
							<input data-switch="true" type="checkbox" checked="checked" data-on-color="info" data-on-text="Aktif" data-off-text="Tidak Aktif" data-off-color="danger" name="status" value="1">
							<?php elseif($users->status == '0'): ?>
							<input type="hidden" name="status" value="0">
							<input data-switch="true" type="checkbox" data-on-color="info" data-on-text="Aktif" data-off-text="Tidak Aktif" data-off-color="danger" name="status" value="1">
							<?php endif; ?>
							<br>
							<span class="m-form__help">Status Yang Sekarang</span>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							<button type="submit" class="btn btn-primary">Update</button>
							<a href="<?php echo e(url('master.user.grid')); ?>"class="btn btn-secondary">Batal</a>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#element").on("change", function(){
			var text = $("#element option:selected").text();
			$(".name").val(text);
		}); 
	});
</script>
<?php $__env->stopSection(); ?>							
<?php echo $__env->make('layouts.masteruser', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>