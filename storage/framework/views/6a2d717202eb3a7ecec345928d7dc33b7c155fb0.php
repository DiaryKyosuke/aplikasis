<?php $__env->startSection('title','Inventory Barang Masuk'); ?>
<?php $__env->startSection('masuk'); ?>
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Inventory Barang Masuk
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="<?php echo e(url('inventory.masuk.form')); ?>" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah Data</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_masuk">
			<thead >
				<tr class="text-center">
					<th width="20px">No.</th>
					<th width="20%">Tanggal</th>
					<th width="40%">Keterangan</th>
					<th width="20%">Hak Akses</th>
					<th width="40%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $__currentLoopData = $transaksi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trans): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(++$i); ?>.</td>
					<td><?php echo e(Carbon\Carbon::parse($trans->tanggal)->formatLocalized('%A, %d %B %Y')); ?></td>
					<td><?php echo e($trans->keterangan); ?></td>
					<td><?php echo e($trans->permission); ?></td>
					<td class="text-center">
						<?php if($trans->permission == 'terkunci'): ?>
						<form action="inventory/masuk/<?php echo e($trans->id_transaksi); ?>/unlock" method="post" enctype="multipart/form-data">
							<?php echo csrf_field(); ?>
							<input type="hidden" name="permission" value="terbuka">

							<a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.masuk.<?php echo e($trans->id_transaksi); ?>.show" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "1000px"}}}'><i class="fas fa-id-card"></i></a>

							<button type="submit" class="btn btn-danger active m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air"><i class="fas fa-lock-open"></i>
							</button>
						</form>
						<?php elseif($trans->permission == 'terbuka'): ?>
						<form action="inventory/masuk/<?php echo e($trans->id_transaksi); ?>/permission" method="post" enctype="multipart/form-data">
							<?php echo csrf_field(); ?>
							
							<input type="hidden" name="permission" value="terkunci">
							
							<a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.masuk.<?php echo e($trans->id_transaksi); ?>.show" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "1000px"}}}'><i class="fas fa-id-card"></i></a>

							<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.masuk.<?php echo e($trans->id_transaksi); ?>.edit"><i class="fas fa-user-edit"></i></a>

							<a href="inventory/masuk/<?php echo e($trans->id_transaksi); ?>/destroy" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air"><i class="fas fa-trash-alt" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus')"></i></a>

							<button type="submit" class="btn btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air"><i class="fas fa-lock"></i>
							</button>
						</form>
						<?php else: ?>
						<?php endif; ?>
					</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.inventorymasuk', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>