<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail extends Model
{
	Protected $table ='detail_transaksi';
	Protected $primaryKey ='id_transaksi';
	Protected $fillable =['id_transaksi','id_barang','harga','jumlah'];
}
