<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
Use App\Barang;
class BarangController extends Controller
{
            /**
     * Create a new controller instance.
     *
     * @return void
     */
            public function __construct()
            {
                $this->middleware('auth');
            }
// function Menampilkan Data Dari Database
            public function barang()
            {
                $data_barang =\App\Barang::latest()->paginate();
                return view('master.barang.grid',['data_barang' => $data_barang])
                ->with('no',(request()->input('page',1)-1)*10);
            }
            public function form()
            {
             return view('master.barang.form');
         }

// function Tambah Data di Tabel Dan function Upload Gambar
         public function tambah(Request $request)
         {
            $this->validate($request,[
                'nama_barang'   =>'required',
                'harga'         =>'required',
                'gambar'        =>'image|mimes:jpg,jpeg,png',
                'stok'          =>'required',
            ]);
            $harga = $request->harga;
            $harga_str = preg_replace("/[^0-9]/", "", $harga);
            $gambar     = $request->file('gambar');
            $namaFile   = $gambar->getClientOriginalName();
            $request    -> file('gambar')->move('uploadgambar',$namaFile);
            $do         = new \App\Barang($request->all());
            $do->harga = $harga_str;
            $do->gambar = $namaFile;
            $do->save();
            return redirect('master.barang.grid')->with('sukses','Selamat Data Yang Anda Inputkan Berhasil Di Tambahkan');
        }

// menuju view edit
        public function edit($id_barang)
        {
            $barang =\App\Barang::find($id_barang);
            return view('master.barang.edit_barang',['barang'=>$barang]);
        }

// function update nya
        public function update(Request $request,$id_barang)
        {
            $this->validate($request,[
                'nama_barang' =>'required',
                'harga'=>'required',
                'gambar'=>'image|mimes:jpg,jpeg,png',
                'stok'=>'',
            ]);
            $barang = Barang::find($id_barang);
            $harga = $request->get('harga');
            $harga_str = preg_replace("/[^0-9]/", "", $harga);

            $barang->nama_barang = $request->get('nama_barang');
            $barang->stok = $request->get('stok');
            $barang->harga = $harga_str;
            $barang->save();
            if ($request->hasFile('gambar')) {
                $request->file('gambar')->move('uploadgambar/',$request->file('gambar')->getClientOriginalName());
                $barang->gambar = $request->file('gambar')->getClientOriginalName();
                $barang->save();
            }

            return redirect('master.barang.grid')->with('sukses','Selamat Data Yang Anda Update Telah Berhasil');
        }
// function Delete nya
        public function delete($id_barang)
        {
            $barang = \App\Barang::find($id_barang);
            $barang->delete();
            return redirect('master.barang.grid')->with('sukses','Sulamat Data Yang Anda Hapus Berhasil');
        }
    }
