<?php

namespace App\Exports;

use App\transaksi;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
class detailExport implements FromView,ShouldAutoSize,WithColumnFormatting
{

	public function __construct(string $tanggal,string $tanggal_akhir,int $jenis)
	{
		$this->jenis = $jenis;
		$this->tanggal = $tanggal;
		$this->tanggal_akhir = $tanggal_akhir;
	}

	public function view(): View
    {
        return view('tampilan', [
            'dets' => transaksi::first()->join('detail_transaksi','detail_transaksi.id_transaksi','=','transaksi.id_transaksi')
		->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
		->where('tanggal','>=',$this->tanggal)
		->Where('tanggal','<=', $this->tanggal_akhir)
		->where('jenis','=', $this->jenis)
		->get()
		->sortBy('tanggal')
        ])->with('i');
    }
    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
        ];
    }
}

