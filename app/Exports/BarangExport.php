<?php

namespace App\Exports;

use App\transaksi;
use App\Barang;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
class BarangExport implements FromView,ShouldAutoSize
{
	public function __construct(string $tanggal,string $tanggal_akhir,int $id_barang)
	{
		$this->id_barang = $id_barang;
		$this->tanggal = $tanggal;
		$this->tanggal_akhir = $tanggal_akhir;
	}

	public function view(): View
	{
		$awal = $this->tanggal;
		$akhir = $this->tanggal_akhir;
		return view('cetak_barang', [
			'dets' => transaksi::first()
			->join('detail_transaksi','detail_transaksi.id_transaksi','=','transaksi.id_transaksi')
			->where('tanggal','>=',$this->tanggal)
			->Where('tanggal','<=', $this->tanggal_akhir)
			->where('id_barang','=',$this->id_barang)
			->get()
			->sortBy('tanggal'),
			'deta' => DB::table('barang')
			->where('id_barang','=',$this->id_barang)
			->first(),
			'tanggalaw' => $this->tanggal,
			'tanggalak'=> $this->tanggal_akhir
		],compact('awal','akhir'))->with('i');
	}
}
